/***************************************************************
* Mail box functions
* A very simple mailbox events for the state machine engine
* for passing information between state machine tasks
****************************************************************
*	C o p y r i g h t   N o t i c e
*	Copyright (c) 2010 - 2012 JMOS
*	All rights reserved.
*
* This source code maintained on GITORIOUS by "forthnutter"
* https://git.gitorious.org/utilities/state_machine_engine.git
*
****************************************************************/

#include <stdlib.h>
#include <stdbool.h>

#include "MailBox.h"

#define MB_FLAG			(1UL << 0)
#define MB_CHAR_FLAG	(1UL << 1)
#define MB_SHORT_FLAG	(1UL << 2)
#define MB_INT_FLAG		(1UL << 3)
#define MB_LONG_FLAG	(1UL << 4)
#define MB_PTR_FLAG		(1UL << 5)


/*********************************************
* Descrition:	 Set mailbox Flag
* Arguments:	Pointer to mailbox structure
* Return:		True for good
*				False for bad
***********************************************/
bool MB_Set_Flag(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		if(!(mb->flags & MB_FLAG))
		{
			mb->flags |= MB_FLAG;
			retval = true;
		}
	}
	return(retval);
}

/**************************************************
* Description:	Clear mailbox flag
* Arguments:	Pointer mailbox structure
* Return:		True for good
**************************************************/
bool MB_Clear_Flag(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		if(mb->flags & MB_FLAG)
		{
			mb->flags = false;
			retval = true;
		}
	}
	return(retval);
}

/***************************************************
* Description:	Get the status of flag
* Arguements:	Pointer to mailbox structure
* Return:		flag status
***************************************************/
bool MB_Flag_Status(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		if((mb->flags & (MB_FLAG | MB_CHAR_FLAG | MB_SHORT_FLAG | MB_LONG_FLAG | MB_PTR_FLAG)) != 0 ) retval = true;
	}
	return(retval);
}

/*****************************************************
* Description:	Read the flag bit
* Arguments:	Pointer to mailbox structure
* Return:		True has been read and deactivated
*****************************************************/
bool MB_Read_Flag(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		if((mb->flags & MB_FLAG) != 0)
		{
			retval = true;
			mb->flags &= ~MB_FLAG;
		}
	}
	return(retval);
}


/*************************************************
 * Description:	Clear byte in mail
 * Arguments:	Pointer to mailbox struct
 * Return:		true if good
 * 				false not good
 *************************************************/
bool MB_Clear_Byte(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		mb->flags &= ~MB_CHAR_FLAG;
		retval = true;
	}
	return(retval);
}

/************************************************
* Description:	write to byte box
* Arguments:	Pointer to mailbox struct
* Return:		True if good
*				false not ready
**************************************************/
bool MB_Write_Byte(MAILBOX_STRUCT *mb, char data)
{
	bool retval = false;

	if(mb != NULL)
	{
		if(!(mb->flags & MB_CHAR_FLAG))
		{
			mb->char_data = data;
			mb->flags |= MB_CHAR_FLAG;
			retval = true;
		}
	}
	return(retval);
}



/*************************************************
 * Description:	Clear byte in mail
 * Arguments:	Pointer to mailbox struct
 * Return:		true if good
 * 				false not good
 *************************************************/
bool MB_Clear_Short(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		mb->flags &= ~MB_SHORT_FLAG;
		retval = true;
	}
	return(retval);
}



/***************************************************
* Description:	Write short box value
* Arguments:	Pointer to mail box structure
*				value to write
* Return:		True if good
****************************************************/
bool MB_Write_Short(MAILBOX_STRUCT *mb, short data)
{
	bool retval = false;

	if(mb != NULL)
	{
		if(!(mb->flags & MB_SHORT_FLAG))
		{
			mb->short_data = data;
			mb->flags |= MB_SHORT_FLAG;
			retval = true;
		}
	}
	return(retval);
}



/*************************************************
 * Description:	Clear byte in mail
 * Arguments:	Pointer to mailbox struct
 * Return:		true if good
 * 				false not good
 *************************************************/
bool MB_Clear_Int(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		mb->flags &= ~MB_INT_FLAG;
		retval = true;
	}
	return(retval);
}



/*************************************************
* Description:	Write long box data
* Arguments:	Pointer to mail box structure
*				value to write
* Return:		True if good
************************************************/
bool MB_Write_Int(MAILBOX_STRUCT *mb, int data)
{
	bool retval = false;

	if(mb != NULL)
	{
		if(!(mb->flags & MB_INT_FLAG))
		{
			mb->int_data = data;
			mb->flags |= MB_INT_FLAG;
			retval = true;
		}
	}
	return(retval);
}

/*************************************************
* Description:	Write long box data
* Arguments:	Pointer to mail box structure
*				value to write
* Return:		True if good
************************************************/
bool MB_Write_Long(MAILBOX_STRUCT *mb, long data)
{
	bool retval = false;

	if(mb != NULL)
	{
		if(!(mb->flags & MB_LONG_FLAG))
		{
			mb->long_data = data;
			mb->flags |= MB_LONG_FLAG;
			retval = true;
		}
	}
	return(retval);
}





/**************************************************
* Description:	Write to a pointer to mailbox
* Arguments:	Pointer to mail box structure
*				Value to write
* Return:		True if good
*************************************************/
bool MB_Write_Ptr(MAILBOX_STRUCT *mb, void *ptr)
{
	bool retval = false;

	if(mb != NULL)
	{
		if(!(mb->flags & MB_PTR_FLAG))
		{
			mb->ptr_data = ptr;
			mb->flags |= MB_PTR_FLAG;
			retval = true;
		}
	}
	return(retval);
}


/***********************************************
* Description:	Read mailbox byte value
* Arguments:	Pointer to mailbox structure
* 				Pointer byte value
* Return:		truen if byte is valid
***********************************************/
bool MB_Read_Byte(MAILBOX_STRUCT *mb, char *value)
{
	bool retval = false;

	if((mb != NULL) && (value != NULL))
	{
		*value = mb->char_data;
		if((mb->flags & MB_CHAR_FLAG) != 0) retval = true;
		mb->flags &= ~MB_CHAR_FLAG;
	}
	return(retval);
}

/**********************************************
* Description:	Read short from mail box
* Arguements:	Pointer to mailbox structure
*				Pointer short
* Return:		True if valid data
**********************************************/
bool MB_Read_Short(MAILBOX_STRUCT *mb, short *value)
{
	bool retval = false;

	if((mb != NULL) && (value != NULL))
	{
		*value = mb->short_data;
		if((mb->flags & MB_SHORT_FLAG) != 0) retval = true;
		mb->flags &= ~MB_SHORT_FLAG;
	}
	return(retval);
}

/******************************************************
* Description:	Read int from mail box
* Arguments:	Pointer to mailbox structure
*				Pointer to int value
* return:		true if data is valid
*****************************************************/
bool MB_Read_Int(MAILBOX_STRUCT *mb, int *value)
{
	bool retval = false;

	if((mb != NULL) && (value != NULL))
	{
		*value = mb->int_data;
		if((mb->flags & MB_INT_FLAG) != 0) retval = true;
		mb->flags &= ~MB_INT_FLAG;
	}
	return(retval);
}



/******************************************************
* Description:	Read long from mail box
* Arguments:	Pointer to mailbox structure
*				Pointer to long value
* return:		true if data is valid
*****************************************************/
bool MB_Read_Long(MAILBOX_STRUCT *mb, long *value)
{
	bool retval = false;

	if((mb != NULL) && (value != NULL))
	{
		*value = mb->long_data;
		if((mb->flags & MB_LONG_FLAG) != 0) retval = true;
		mb->flags &= ~MB_LONG_FLAG;
	}
	return(retval);
}

/******************************************************
* Description:	Read pointer from mail box
* Arguments:	Pointer to mailbox structure
*				Pointer to pointer
* return:		true if data is valid
*****************************************************/
bool MB_Read_Ptr(MAILBOX_STRUCT *mb, void **ptr)
{
	bool retval = false;

	if((mb != NULL) && (ptr != NULL))
	{
		*ptr = mb->ptr_data;
		if((mb->flags & MB_PTR_FLAG) != 0) retval = true;
		mb->flags &= ~MB_PTR_FLAG;
	}
	return(retval);
}



/******************************************************
* Description:	Read pointer from mail box
* Arguments:	Pointer to mailbox structure
*				Pointer to pointer
* return:		true if data is valid
*****************************************************/
bool MB_Clear_Ptr(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		mb->flags &= ~MB_PTR_FLAG;
		retval = true;
	}
	return(retval);
}



/********************************************
* Description:	Init Mailbox
* Arguments:	Pointer to Mailbox Structure
* Returns:		True for good
*				False is bad
********************************************/
bool MB_Init(MAILBOX_STRUCT *mb)
{
	bool retval = false;

	if(mb != NULL)
	{
		mb->flags = 0;
		mb->char_data = 0;
		mb->short_data = 0;
		mb->int_data = 0;
		mb->long_data = 0;
		mb->ptr_data = NULL;
	}
	return(retval);
}

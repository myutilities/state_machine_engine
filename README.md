# State Machine Engine

**1. Introduction**

The state machine engine consists of a main **super loop** that calls functions (tasks) modeled as state machines.Critical real-time events handled in the background by using interrupts or dedicated hardware. Tasks communicate with one another through global data structures called mailboxes. A timer Interrupt Service Routine bumps a global counter each time, this global counter is used to manage timers.

**1.1 Main Loop**

All tasks are structure into a list a list module is used “List.c” and “List.h”.  
Each task runs in turn within an endless loop as shown below, has the responsibility of returning as soon as possible so that other tasks can run. In the background, interrupts or dedicated hardware handle time critical events. The State Machine Engine calls tasks via a task list as shown below.

```c
while(CurrentTask)  
{  
    tp = CurrentTask;  
    if(tp->taskPtr != NULL)  
    {  
        tp->taskPtr(tp->instPtr);  
    }  
    CurrentTask = CurrentTask->next;
}
```

**1.2 Task Initializations**

Typically tasks need to perform some initializations on startup. The State Machine Engine calls the task initialization functions before entering the main loop as shown below. If a state machine requires no initializations, the pointer taskInitPtr can be set to NULL.

```c  
/* Do startup initializations. */  
tp = CurrentTask;  
while (tp->taskPtr != NULL)  
{
  if (tp->taskInitPtr != NULL) tp->taskInitPtr(tp->instPtr);
  tp++;
}
```
**1.3 Instance Data**

In the code above the argument to the task is a pointer to a C struct called the task's instance data. Instance data allows tasks to be coded generically. Consider the task below which reads data from an IO port and displays the data at a given screen location.

```c  
typedef struct  
{
   int portNum;
   int x;
   int y;
 } typeIoTaskData;
 
 void ioTask(typeIoTaskData * d)
 {
   int data;

   data = readPort(d->portNum);
   displayData(d->x, d->y, data);
 }
```

Note that the task will read from a different port and write to a different screen location depending upon the values of the instance data variables d->x, d->y, and d->portNum. So, for example, we could call ioTask 3 times, with 3 different instance data structures. Each structure would have a different value for each of the instance data variables d->x, d->y, and d->portNum. The effect would be that the task would read from a different port, and write to a different xy screen location each time it is called. This allows us to write one piece of code and change its behavior by passing it a particular instance data structure.

**1.4 Inter-task Communication**

Although tasks run autonomously, they sometimes need to communicate with one another. State machine tasks communicate through global data structures called mailboxes. How mailboxes are used is described in the chapter "Case Study 1".

**1.5 Timer Management**

Most real-time systems require timers. A timer allows a task to wait for a specific period of time to pass. The State Machine Engine provides functions for setting and checking timers. A timer interrupt service routine bumps a global counter (Ticks) every 10 milliseconds. Timers are started and checked using the smStartTimer and smTimeout functions that are described later.

**2 Data Structures**

The basic data structures used by the State Machine Engine are covered in this chapter.

**2.1 Task Table**

The task table is an array of "typeTask" elements.

```c
typedef struct  
{
	void * instPtr;
	void (* taskInitPtr)(void *);
	void (* taskPtr)(void *);
} typeTask;
```

The structure member "instPtr" points to the task instance data, "taskInitPtr" points to the task initialization function, and "taskPtr" points to the task function itself. The global array "TaskTable" is an array of "typeTask" structures. The main loop cycles through the TaskTable array, calling each task, and passing it a pointer to its instance data as shown below.

```c
for (;;)  
{
  tp = TaskTable;
  while (tp->taskPtr != NULL)  
  {
    tp->taskPtr(tp->instPtr);
    tp++;
  }
}
```

**2.2 Mailboxes**

Mailboxes are used for inter-task communication. 

```c
typedef struct structMbox  
{
	unsigned int flags;
	int iData;
	long lData;
	void * pData;
} typeMbox;
```

Each task must define an array of mailboxes as a member of the task's instance data structure.

```c
#define LEN_TEST_TASK_MAIL	2
#define TASK_A_FIRST_MBOX	0
#define TASK_A_SECOND_MBOX	1

typedef struct structTaskAInstanceData
{
  int state;
  typeMbox mail[LEN_TEST_TASK_MAIL];
} typeTaskAInstanceData;
```

Below we define the instance data for taskA.

```c
typeTaskAInstanceData TaskAInstanceData =
{
    0,       			/* state */
    (0, 0, 0L, NULL),	/* First mailbox. */
    (0, 0, 0L, NULL),	/* Second mailbox. */
};
```

The sample code below shows how taskB puts a mail message into taskA's first mailbox. The iData mailbox member is set to 5.

```c
sendMailI(TaskAInstanceData, TASK_A_FIRST_MBOX, 5);
```

TaskB checks mail in the first mailbox as shown below. When mail is detected the integer data value is removed, and the mailbox flag cleared.

```c
if (haveMail(TaskAInstanceData, TASK_A_FIRST_MBOX))
{
    printf("Integer received is %d\n",     TaskAInstanceData.mail[TASK_A_FIRST_MBOX].iData);
    clrMail(TaskAInstanceData, TASK_A_FIRST_MBOX);
}
```

**2.3 Task Instance Data Structures**

It is recommended that each task have an instance data structure. The data structure members are task dependent and are chosen by the programmer. The data structure must contain the task's state; other instance data members are added as needed.  
If your application will never have multiple instances of the same task, then the instance data member of the typeTask structure can be NULL, and instance data can be locally data defined within the task function itself. Variables that must retain their value across calls (like state) must be declared as static.

**3	Case Study 1**

**3.1 Introduction**

This chapter explains how the State Machine Engine is applied to the solution of a real-time problem. The problem is defined, analyzed, broken down into tasks, and coded. The concept of a task instance is applied to reduce the amount of coding required. Finally, a timing analysis is presented which shows that the State machine approach can be quite effective for this and similar applications.

**3.2 Problem Definition**

Consider an LCD automotive dashboard that displays vehicle speed, fuel level, oil pressure, water temperature, and alternator voltage. All readings are displayed on gauges using graphics on the LCD panel . When any of the readings fall outside an acceptable range, a warning lamp is lit, and a warning tone is generated. There is a separate warning lamp for each reading but only one tone generator and the tone is the same for all alarms. The problem is solved by breaking it down into the tasks outlined below. 

**3.3 Data Acquisition Task**

The data acquisition task periodically acquires the engine data from IO ports and saves the data in a global table. The task utilizes the instance data pointer that it receives to obtain the instance specific port number. It also uses the instance data to determine the time delay between readings. The task is driven by the instance data and therefore can be used for all five IO ports (speed, fuel level, oil pressure, water temperature, and alternator voltage). The data acquisition instance data structure is shown below.

**3.3.1 Data Acquisition Task Instance Data**

```c
/* Data Acquisition Instance Data */
typedef struct
{
   int state;   /* Task state */
   long delay;  /* Sample time in ms */
   long timer;  /* Holds time-out count */
   int portNum; /* Port number to read */
   int instNum; /* Instance number */
} typeDataAcq;
```

The task receives as its argument a pointer to a structure of type typeDataAcq shown above. state holds the task state and is used with a switch statement in the task to resume where it left off. delay holds the timer count. On each time-out, the data is read from portNum and stored into a global table named GaugeReading that is indexed by instNum.  As shown below, the task is implemented generically. It will be called five times from within the main loop, each time with a different instance data pointer.

**3.3.2 Data Acquisition Initialization Task**

```c
void dataAcqInit(typeDataAcq * d)
{
  d->timer = smStartTimer(d->delay);  /* Start the timer. */
}
```

**3.3.3 Data Acquisition Task**

```c
void dataAcq(typeDataAcq * d)
{
   switch (d->state)
   {
       case 0:
        if (smTimeout(d->timer))
        {
            GaugeReading[d->instNum] = readGauge(d->portNum);
            d->timer = smStartTimer(d->delay);
        }
        break;
   }
}
```

**3.4 Data Display Task**

The Data Display task initially draws the gauges on the LCD display, and then updates the gauge reading periodically. Each gauge has a different update rate. As with the Data Acquisition task, this task can be implemented generically and driven by instance data as shown below.

**3.4.1 Display Task Instance Data**

```c
typedef struct
{
   int state;   /* Task state */
   long delay;  /* Display update time in ms */
   long timer;  /* Holds time-out count */
   int instNum; /* Instance number */
} typeDataDisp;
```

**3.4.2 Display Initialization Task**

```c
void dataDispInit(typeDataDisp * d)
{
  drawGauge(d->instNum);
  d->timer = smStartTimer(d->delay);
}
```

**3.4.3 Display Task**

```c
void dataDisp(typeDataDisp * d)
 {
   switch (d->state)
   {
       case 0:
        if (smTimeout(d->timer))
        {
            updateGauge(d->instNum);
            d->timer = smStartTimer(d->delay);
        }
        break;
   }
}
```

Function updateGauge accesses the latest reading by using d->instNum to index into table GaugeReading. The instance number is also used to determine which LCD display gauge to update.

**3.5 Warning Monitor Task**

The Warning Monitor Task monitors each engine reading for an out of range condition. When an out of range condition is detected an indicator lamp is lit and a signal is sent to the Tone Generator Task to sound the alarm.

**3.5.1 Warning Monitor Instance Data**

```c
typedef struct
{
   int state;   /* Task state */
   long delay;  /* Sample time in ms */
   long timer;  /* Holds time out count */
   int instNum; /* Instance number */
   int min;     /* Min allowable reading */
   int max;     /* Max allowable reading */
 } typeWarningMon;
 ```

The warning monitor instance data requires two addition fields: the minimum and maximum allowable reading values. 

```c
void warningMonInit(typeDataAcq * d)
{
  d->timer = smStartTimer(d->delay);  /* Start the timer. */
}
```

**3.5.3 Warning Monitor Task**

```c
#define outOfRange(x,id)  (x >min || x > id->max)
 
void warningMon(typeWarningMon * d)
{
   int reading;
 
   switch (d->state)
   {
       case 0:
        if (smTimeout(d->timer))
        {
            reading = GaugeReading[d->instNum];
            if (outOfRange(reading, d))
            {
                warningLampOn(d->instNum);
                SoundOn++;
                d->state = 1;
            }
        }
        else
        {
            d->timer = smStartTimer(d->delay);
        }
        break;
 
       case 1:
        reading = GaugeReading[d->instNum];
        if (!outOfRange(reading, d))
        {
            warningLampOff(d->instNum);
            SoundOn--;
            d->state = 0;
        }
        break;
    }
}
```
**3.5.4 Local Data vs. Instance Data**

Note that the variable *reading* is not part of the instance data structure pointed to by *d*. Any temporary variable that does not have to maintain its value between task calls can be declared locally. Contrast *reading* with *state* or *timer* which must retain their values between calls. It is important to understand that local *static* variables cannot be used for *state* and *timer* either because there must be a copy of *state* and *timer* for **each** task instance.  
**warningLampOn** and **warningLampOff** control the indicated warning lamp.  
These functions might be implemented as follows.

```c
void warningLampOn(int lampNum) 
{
   outputb(LAMP_BASE_ADDR + lampNum, 1);
}
 
void warningLampOff(int lampNum) 
{
   outputb(LAMP_BASE_ADDR + lampNum, 0);
}
```

**3.6 Warning Tone Generator Task**

Assume that there is one tone generator that must be shared among all the gauges. Each time a Warning Monitor Task instance wants to sound the alarm, it increments a global counter called SoundOn. To turn the sound off, SoundOn is decremented. This approach allows all Warning Monitor Task instances to share the single tone generator.
Note that tones are generated properly even though we only have a single instance of the warning tone generator task. For example, if two warnings are detected SoundOn will be incremented twice, once by each task instance of the warning monitor task. When one reading returns to an allowable range, SoundOn will be decremented to 1, but since SoundOn is still non-zero, the warning tone generator task will keep the tone on. 
The warning tone task requires only one instance, so local function variables can be used as instance data. The instance data argument "d" is required only for proper compilation.

**3.6.1 Warning Tone Task**

```c
void warningTone(void * d)
{
   static int state = 0;
 
   switch (state)
   {
       case 0:
        if (SoundOn > 0)
        {
            toneOn();
            state = 1;
        }
        break;
 
       case 1:
        if (SoundOn <= 0)
        {
            toneOff();
            state = 0;
        }
        break;
    }
} 
```

**3.7 Initializing Task Instance Data Structures**

```c
/* Instance Data for all Tasks  */
typeDataAcq DataAcqId[] =
{
    /* State, delay, timer, instance number */
    0, 100L, 0, 0, /* Vehicle speed */
    0, 1000L, 0, 1, /* Fuel Level */
    0, 100L, 0, 2, /* Oil Pressure */
    0, 1000L,0, 3, /* Water temperature */
    0, 100L, 0, 4  /* Alternator volts */
};
 
typeDataDisp DataDispId[] =
{
    /* State, delay, timer, instance number */
    0, 100L, 0, 0, /* Vehicle speed */
    0, 1000L, 0, 1, /* Fuel Level */
    0, 100L, 0, 2, /* Oil Pressure */
    0, 1000L, 0, 3, /* Water temperature */
    0, 100L, 0, 4  /* Alternator volts */
};
 
typeWarningMon WarningMonId[] =
{
    /* State,delay,timer,inst number,min,max */
    0, 100L, 0, 0, 0, 100,  /* Vehicle speed */
    0, 1000L, 0, 1, 1, 100, /* Fuel Level */
    0, 100L, 0, 2, 10, 90, /* Oil Pressure */
    0, 1000L, 0, 3, 0, 220, /* Water temp */
    0, 100L, 0, 4, 13, 18  /* Alternator volt */
};
```

**3.8 Instance Data Characteristics**

+ All state variables are initialized to zero. This will force each task to enter state 0 when first called. 
+ Data is sampled at different rates. For example, fuel level, which changes very slowly is sampled every 1000 system clock ticks while vehicle speed is sampled every 100 ticks.
+ Each gauge has a different range of acceptable values. Oil pressure may range in value from 10 to 90, while water temperature ranges from 0 to 220.
+ An instance number is contained in the instance data. The instance number is used in function  warningLampOn to determine which lamp to turn on. It is used in task dataAcq as an index into table GaugeReading.

**3.9 Initializing the Task Table**

Recall that the task table was defined in a previous chapter as shown below.

```c
typedef struct
{
  void * instPtr;
  void (* taskInitPtr)(void *);
  void (* taskPtr)(void *);
} typeTask;
```

**instPtr** points to the instance data, **taskINitPtr** points to the initialization task, and taskPtr points to the task (C function). The code below defines and initializes the task table. Note that since the warningSound task requires no instance data its instance data pointer is NULL. The task table is terminated by two NULL's.

There are two ways to start tasks. One is to fill the task table as we have done in this example (see below). The other is to use the system call startSmTask. The system call startSmTask has the advantage of allowing tasks to be added to the task table dynamically during program execution.

```c
typeTask TaskTable[] =
{
   &DataAcqId[0], dataAcqInit, dataAcq
   &DataAcqId[1], dataAcqInit, dataAcq
   &DataAcqId[2], dataAcqInit, dataAcq
   &DataAcqId[3], dataAcqInit, dataAcq
   &DataAcqId[4], dataAcqInit, dataAcq
 
   &DataDispId[0], NULL, dataDisp,
   &DataDispId[1], NULL, dataDisp,
   &DataDispId[2], NULL, dataDisp,
   &DataDispId[3], NULL, dataDisp,
   &DataDispId[4], NULL, dataDisp,
 
   &WarningMonId[0], warningMonInit, warningMon,
   &WarningMonId[1], warningMonInit, warningMon,
   &WarningMonId[2], warningMonInit, warningMon,
   &WarningMonId[3], warningMonInit, warningMon,
   &WarningMonId[4], warningMonInit, warningMon,
 
   NULL, NULL, warningTone,
 
   NULL, NULL, NULL  /* Terminate table */
};
```

**3.10 Starting and Initializing**

The main loop is implemented as shown below. initTimer initializes the timer chip and the interrupt vector table; this sets up the timer interrupt service routine that increments the global counter used by smStartTimer and smTimeout.

```c
/*  Main Engine Loop */
 
void main(void)
{
   smInit(PC_10MS, 10000);		/* Init timer. */
   smEngine();			/* Enter main loop. */
}
```

**3.11 Timing Analysis**

It is instructive to analyze the loop latency time. The following assumptions are made:

+ Time spent inside the timer isr is negligible.
+ The overhead incurred in the main loop to dispatch a function is about 10 machine instructions.
+ The maximum number of instructions performed in any single task  is 100 machine instructions.

The worst case total number of instructions executed per task call is then 110. There are 16 tasks in the task table. The loop latency time is the time required to call all of the tasks in the task table. This value is computed below, assuming 1 microsecond per instruction.  

(110 instructions per task)   X   (16 tasks)   X   (1 microsecond per instruction) = 1760 microseconds

The loop latency time is about 2 milliseconds. This means that once a task is called, about 2 milliseconds will have elapsed before it is called again. For this application, 2 milliseconds is more than adequate. No critical events will be missed in any of the tasks because of a 2 millisecond latency. In fact, for this application, a latency time of 100 milliseconds would be acceptable. A latency time beyond 100 milliseconds may still be acceptable. The gauges may operate a little slower than desirable, but the point is that errors would not be introduced into the system as a result of excessive latency time.

**3.12 Excessive Latency may not be Harmful**

The point made above is an important one: for many real-time applications, excessive latency is not harmful. That is, it will not introduce errors into the system. Screen updates may slow down, and data may not be acquired as fast as desired, but errors are not introduced.  This point should always be kept in mind.

**3.13 When Excessive Latency is Harmful**

Excessive latency is harmful when it introduces errors. Suppose that a task monitors an incoming serial communications line that transmits data at 1000 bytes per second, or 1 byte per millisecond. The purpose of the task is to read and process the incoming data. If data is received once each millisecond, and the latency time is 5 milliseconds, then data will be lost and errors will occur. In situations like this alternatives must be found. In this example, the solution is to process data inside the isr, or slow down the data rate.

**3.14 Comments**

It may appear that this application could have been simplified by putting all the code into one large loop. All readings, updates, range checks, and so forth could be done in one module. The problem with that kind of approach is that it can become unmanageable. The approach outlined in this chapter provides manageability and room for modification. It also promotes modular design and makes debugging and problem isolation easier.

One may also question why we need to read the engine data and store it into a global array. I.e., why can't the display task just read the IO ports directly? The answer is that it could. In the real world however, the readings would need to be converted from a DAC reading to, for example, speed in MPH. It is better to let a separate task handle this conversion and store the value for the display task into the global array. There may also be some filtering or averaging required which, again, is better handled by a separate task.
Also note that many of the timers are not really necessary. The data acquisition and warning tasks, for example, could sample and check data each time they are called, thus eliminating the timer. However, time consuming tasks like display update should wait on a timer. In fact, display update could be changed to update the gauge only when the new reading is different. Many such changes and optimizations are possible.
Real-time problems are approached by breaking the problem down into separate tasks. Typically, each task performs one function. If a task is performing multiple functions, it may be better to break it down into separate tasks. Where possible, multiple instances of the same task should be used. Many real-time problems do not have hard real-time constraints. For this class of problem excessive latency will not introduce errors into the system, and is generally not a concern. 
In general, real-time software implementation is an iterative process. As the system comes together fine tuning and changes are usually required.  

**4 Case Study 2**

**4.1 Introduction**

This example is a bit more involved than Case Study 1. It also shows how mailboxes are used and introduces the concept of a system reset.

**4.2 Problem Definition**

This is a simple MS-DOS based character graphics game application using Borland's Turbo C/C++ version 1.01, which Borland has made freely downloadable from its site (see Appendix A). 
As bombs fall vertically and randomly from the sky the user can move a gun at the bottom of the screen horizontally, thus allowing him to position the gun under a bomb and then fire a missile at the bomb by pressing the space bar. If the missile hits the bomb a sound is heard and the bomb disappears. A sound is also heard when the gun is fired, and as the bombs move down. 
The game starts out at Level 1 and progresses to more difficult levels as the user gets better. We will implement a simple two level scheme. At Level 2 bombs fall faster and are able to dodge bullets. We'll decide on the details as we go.

**4.3 Task Breakdown**

First we break the system down into the following tasks: a gun task, a bomb task, and a bullet task. 

**4.3.1 Task Initializations**

In this case study we choose not to use the taskInitPtr in order to show an alternative approach. In this approach the state machine starts in an idle state. The state machine will remain in the idle state until it receives mail in a particular mailbox. When the mail is received the task performs whatever initializations it requires, then enters an active state. 

**4.3.2 Gun Task**

**4.3.3 Bomb Task**

**4.3.4 Bullet Task**

**4.4 Task Instance Data**

**4.4.1 Gun Task Instance Data**

**4.4.2 Bomb Task Instance Data**

**4.4.3 Bullet Task Instance Data**

**4.5 Mailboxes**

**4.6 Generating Sound**

**4.7 System Reset**

**5 System Calls**

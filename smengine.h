/*******************************************************
*	C o p y r i g h t   N o t i c e
*	Copyright (c) 2010 - 2012 JMOS
*	All rights reserved.

*	Copyright (c) 2007 - 2009 JMOS
*	All rights reserved.

* 	Copyright (c) 1991 - 2003 Tics Realtime.
*	All rights reserved.
*
*	2012 02 29  JLM:
*	Minor changes for use in KEIL MDK
*	Used in Imdex logger
*
*	2007 11 09	JLM:
*	This state machine engine is now heavly mogified to reduse
*	memory usage by using a list functions
*	Used in Computronics Ltd. electronics signs
********************************************************/
#ifndef _SMENGINE_H
#define _SMENGINE_H


/*******************************************************
*  I n c l u d e s
*******************************************************/
//#include <stdio.h>
//#include <stdlib.h>
#include <stdbool.h>
#include "list.h"
#include "MailBox.h"

/*******************************************************
*  D e f i n e s
*******************************************************/
#define OK		0		/* Successful call */
#define NOT_OK 	!OK		/* An error occurred */

#define SMEVERSION	4	/* may check this version number some day */

/*******************************************************
*	E x t e r n s
*******************************************************/

/*******************************************************
* Global Structures
*******************************************************/
typedef struct
{
	LIST_ITEM list;					//now this structure belongs to a list
	bool reset;						//restart the task again
	bool remove;                  // to run task once
	void * instPtr;					//pointer to a structure
	void (* taskInitPtr)(void *);	//pointer to Start routine
	void (* taskPtr)(void *);		//pointer to State Machine task
}SME_STRUCT;

/*******************************************************
*	M a c r o s
******************************************************/


/*******************************************************
*	P r o t o t y p e s
******************************************************/
extern void SME_Init(void);		/* initilise the state machine */
extern bool SME_AddTask(void (* taskPtr)(void *), void (* taskInitPtr)(void *), void * instPtr,SME_STRUCT *sme);
extern bool SME_RemoveTask(SME_STRUCT *sme);
extern void SME_Restart(void);
extern void SME_Execute(void);	/* this is the dispatcher loops forever or until list is empty */
extern void SME_Execute_rc(void);	/* This is a test dispatcher */
#endif


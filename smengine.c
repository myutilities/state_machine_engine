/*******************************************************
*	C o p y r i g h t   N o t i c e
*	Copyright (c) 2010 - 2012 JMOS
*	All rights reserved.
*
* This code maintained on GITORIOUS by "forthnutter"
* https://git.gitorious.org/utilities/state_machine_engine.git
*
*	Copyright (c) 2007 - 2009 JMOS
*	All rights reserved.
*
*	Copyright (c) 1991 - 2003 Tics Realtime.
*	All rights reserved.
*	
*	2012 02 29  JLM:
*	Minor changes for use in KEIL MDK
*	Used in Imdex logger
*
*	2007 11 09	JLM:
*	This state machine engine is now heavly mogified to reduse
*	memory usage by using a list functions
*	Used in Computronics Ltd. electronics signs
********************************************************/

/*******************************************************
*  I n c l u d e s
*******************************************************/
#include <stdbool.h>
#include <stdio.h>
#include "list.h"		/* I like this general purpose list */
#include "smengine.h"



int SME_Reset= false;		 		/* State machine Global reset */
LIST_ITEM *CurrentTask = NULL;		/* make list task pointer */
LIST_ITEM *CompletedTask = NULL;    /* make list of used task */

/***************************************************************
* Description:		The SM Engine calls each task in round
*					robin fashion.
*					Tasks are structures that contain function
*					pointer and are contained in a list.
***************************************************************/
void SME_Execute(void)
{
	LIST_ITEM *tlist = 0;	//temp list
	LIST_ITEM *glist = 0;	//general list Item
	SME_STRUCT *tp;
	
	while(CurrentTask != NULL)
	{
		if(SME_Reset)
		{
			//do the start task first
			while(CurrentTask != NULL)
			{
				glist = CurrentTask; // get the first task
				CurrentTask = List_RemoveItem(CurrentTask,glist); //remove the first from list
				if((tp = (SME_STRUCT *)glist->owner) != NULL)
				{
					if(tp->taskInitPtr != NULL)
					{
						tp->taskInitPtr(tp->instPtr);
					}

					if(!tp->remove)
					{
					   tlist = List_AddItem(tlist,glist);
					}
				}
			}
			SME_Reset = false;
		}
		else
		{
			while(CurrentTask != NULL)
			{
				glist = CurrentTask;
				CurrentTask = List_RemoveItem(CurrentTask,glist);
				if((tp = (SME_STRUCT *)glist->owner) != NULL)
				{
					if(tp->taskPtr != NULL)
					{
						tp->taskPtr(tp->instPtr);
					}

					if(!tp->remove)
					{
					   tlist = List_AddItem(tlist,glist);
					}
				}
			}
		}
		/*simple swap of pointers if condition is correct */
		if((CurrentTask == NULL) && (tlist != NULL))
		{
			glist = CurrentTask;	//glist has the current list normally NULL
			CurrentTask = tlist;	//Current list now has the address of tlist
			tlist = glist;			//tlist has the glist which is NULL
		}
	}
}


/***************************************************************
* Description:
*        		The State Machine Engine run to completion
*        		calls each task in round robin fashion this
*        		is a test run to make each	task handle its own reset state.
*					Tasks are structures that contain function
*					pointer and are contained in a list.
***************************************************************/
void SME_Execute_rc(void)
{
	LIST_ITEM *tlist = 0;	//temp list
	SME_STRUCT *tp;

   if(SME_Reset)  // see if we are in global reset
   {
      if(CurrentTask != NULL)
      {
         //get the first task
         tlist = CurrentTask; // get the first task
         CurrentTask = List_RemoveItem(CurrentTask,tlist); //remove the first from list
         if((tp = (SME_STRUCT *)tlist->owner) != NULL)
         {
            if(tp->taskInitPtr != NULL)
            {
               tp->taskInitPtr(tp->instPtr);	/* run initilise task */
               CompletedTask = List_AddItem(CompletedTask,tlist);    /* task completed */
            }
         }
      }
      else  // we have an empty current task
      {
         if(CompletedTask != NULL)  //if we have nothing in current see if completed has something
         {
            tlist = CurrentTask;          //glist has the current list normally NULL
            CurrentTask = CompletedTask;  //Current list now has the address of tlist
            CompletedTask = tlist;        //tlist has the glist which is NULL
         }
         SME_Reset = false;               // global reset has finished
      }
   }
   else // we are not in global reset
   {
      if(CurrentTask != NULL)
      {
         tlist = CurrentTask;             // get next task
         CurrentTask = List_RemoveItem(CurrentTask,tlist);
         if((tp = (SME_STRUCT *)tlist->owner) != NULL)
         {
            if(tp->remove == false) // someone requested removeal
            {
               if(tp->reset) // task reset
               {
                  if(tp->taskInitPtr != NULL)
                  {
                     tp->taskInitPtr(tp->instPtr); /* run reset task */
                     tp->reset = false;
                  }
               }
               else
               {
                  if(tp->taskPtr)
                  {
                     tp->taskPtr(tp->instPtr);     /* task */
                  }
               }
               CompletedTask = List_AddItem(CompletedTask,tlist);    /* task completed */
            }
         }
      }
      else
      {
         if(CompletedTask != NULL)
         {
            tlist = CurrentTask;          //glist has the current list normally NULL
            CurrentTask = CompletedTask;  //Current list now has the address of tlist
            CompletedTask = tlist;        //tlist has the glist which is NULL
			}
      }
	}
}




/*************************************************************
* Description:		Resets the state machine engine.
*					All state machine initialization routines
*					are called, then the main state machine
*					engine loop is entered.
**************************************************************/
void SME_Restart(void)
{
	SME_Reset = true;
}


/****************************************************************************************
* Description:		Add a task
* Arguments:		taskPtr - Function Pointer to the task.
*					taskInitPtr - Function Pointer to initilise the task
*					instPtr - Pointer to the task's data structure
*					sme - Pointer to state machine engine task structure
* Returns:			True if data was successfuly applied to the list
*					False if something went wrong e.g. sme was a NULL Pointer
* Note:				basically all data is loaded in sme structure and then added to list
*****************************************************************************************/
bool SME_AddTask(void (* taskPtr)(void *), void (* taskInitPtr)(void *), void * instPtr, SME_STRUCT *sme)
{
	bool retval = false;

	if(sme != NULL)
	{
		sme->reset = false;				/* make when adding task we are initialise it */
		sme->remove = false;          /* this to for running task once and remove after */
		sme->instPtr = instPtr;			/* store data to sme */
		sme->taskPtr = taskPtr;			/* store task function pointer */
		sme->taskInitPtr = taskInitPtr;	/* store task start function pointer */
		sme->list.owner = sme;			/* make sure list knows who is the owner */
		CurrentTask = List_AddItem(CurrentTask, &sme->list);	/* plonk it into the big list */
		retval = true;					/* all good */
	}
	return(retval);
}


/************************************************************************************
* Description:	Remove task
* Arguments:	Pointer SME Struct
* Return:		True successful
* 				False not good
*************************************************************************************/
bool SME_RemoveTask(SME_STRUCT *sme)
{
	bool retval = false;

	if(sme != NULL)
	{
	   sme->remove = true;
		retval = true;
	}
	return(retval);
}

/*********************************************************
* Description:	Initilise the State Machine Engine
* Note:			we need to do this to make sure every thing
*				is ready to run
*********************************************************/
void SME_Init(void)
{
	SME_Reset = true;
	CurrentTask = NULL;
}


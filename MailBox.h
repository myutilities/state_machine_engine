/**********************************************
* Header Mailbox
*********************************************/
#ifndef MAILBOX_H
#define MAILBOX_H

typedef struct
{
	unsigned int flags;
	char char_data;
	short short_data;
	int int_data;
	long long_data;
	void *ptr_data;
}MAILBOX_STRUCT;

extern bool MB_Set_Flag(MAILBOX_STRUCT *mb);
extern bool MB_Clear_Flag(MAILBOX_STRUCT *mb);
extern bool MB_Read_Flag(MAILBOX_STRUCT *mb);
extern bool MB_Flag_Status(MAILBOX_STRUCT *mb);
extern bool MB_Write_Byte(MAILBOX_STRUCT *mb, char data);
extern bool MB_Write_Short(MAILBOX_STRUCT *mb, short data);
extern bool MB_Write_Int(MAILBOX_STRUCT *mb, int data);
extern bool MB_Write_Long(MAILBOX_STRUCT *mb, long data);
extern bool MB_Write_Ptr(MAILBOX_STRUCT *mb, void *ptr);
extern bool MB_Read_Byte(MAILBOX_STRUCT *mb, char *value);
extern bool MB_Read_Short(MAILBOX_STRUCT *mb, short *value);
extern bool MB_Read_Int(MAILBOX_STRUCT *mb, int *value);
extern bool MB_Read_Long(MAILBOX_STRUCT *mb, long *value);
extern bool MB_Read_Ptr(MAILBOX_STRUCT *mb, void **ptr);
extern bool MB_Clear_Short(MAILBOX_STRUCT *mb);
extern bool MB_Clear_Int(MAILBOX_STRUCT *mb);
extern bool MB_Clear_Ptr(MAILBOX_STRUCT *mb);
extern bool MB_Init(MAILBOX_STRUCT *mb);

#endif
